module gitlab.com/golang-package-library/elasticsearch

go 1.17

require (
	github.com/elastic/go-elasticsearch/v8 v8.2.0
	gitlab.com/golang-package-library/env v0.0.0-20220522085307-1f1e68f337d0
	gitlab.com/golang-package-library/logger v0.0.0-20220522181613-012fdadbbc7d
)

require (
	github.com/elastic/elastic-transport-go/v8 v8.1.0 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.21.0 // indirect
)
